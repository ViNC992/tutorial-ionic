angular.module('songhop.services', ['ionic.utils'])

.factory('Recommendations', function($http, SERVER, $q) {

  // variabile che ci servirà per il tag Audio
  var media;

  //creo un'array vuoto che ospiterà gli elementi provenienti dal server
  var o = {
    queue: []
  };

  //metodo che recupera o manda in play la canzone corrente
  o.init = function() {

    if(o.queue.length === 0) {
      //se non c'è nulla in coda riempila
      return o.getNextSongs();
    } else {
      //altrimenti riproduci la song corrente
      return o.playCurrentSong()
    }
  }

  o.playCurrentSong = function() {

    //controllo lo stato di attività della chiamata asincrona
    var defer =$q.defer();

    //avviare la canzone corrente
    media = new Audio(o.queue[0].preview_url);

    // quando il caricamento è completato aziona la promise(risultato operazione asincrona)
    media.addEventListener("loadeddata", function(){
      defer.resolve();
    });

    media.play();

    return defer.promise;

  }

  // metodo quando passiamo alla finestra favoriti
  o.haltAudio = function() {
    if (media) media.pause();
  }

  //creo una funzione con metod get che va a prendere gli elementi dal server
  o.getNextSongs = function() {
   return $http({
     method: 'GET',
     url: SERVER.url + '/recommendations'
   }).success(function(data){
     // inserisco i dati all'interno dell'array vuoto
     o.queue = o.queue.concat(data);
   });
 }

  //creo un metodo che mi permette di procedere alla nuova canzone
  o.nextSong = function() {
    //Elimino l'index of 0
    o.queue.shift();

    // termino la canzone per tenerla a quella corrente
    o.haltAudio();

    //Riempio il box con la coda(queue)
    if (o.queue.length <= 3) {
      o.getNextSongs();
    }
  }

  return o;
})

// creiamo un servizio che chiama recupera ed elimina le canzoni
.factory('User', function($http, $q, $localstorage, SERVER) {

  // associo ad una variabile un'array vuoto che chiamo favorites
  var o = {
    username: false,
    session_id: false,
    favorites: [],
    newFavorites: 0
  }

  // verificare se esiste una sessione utente
  o.checkSession = function() {
    var defer = $q.defer();

    if (o.session_id) {
      //se la sessione è già inizializzara nel servizio
      defer.resolve(true);
    } else {
      //Verificare se una sessione è già salvata nel localstorage di uso precedente
      // se lo è, può utilizzare il servizio
      var user = $localstorage.getObject('user');

      if(user.username) {
        // se p un'utente loggato può prendere la sua lista preferiti dal server
        o.setSession(user.username, user.session_id);
        o.populateFavorites().then(function() {
          defer.resolve(true);
        });
      } else {
        // rifiuta se non sono presenti dati in localstorage
        defer.resolve(false);
      }
    }

    return defer.promise;
  }

  // settare una sessione dati
  o.setSession = function(username, session_id, favorites) {
    if (username) o.username = username;
    if (session_id) o.session_id = session_id;
    if (favorites) o.favorites = favorites;

    //impostare i dati in un'oggetto nel localstorage
    $localstorage.setObject('user', { username: username, session_id: session_id });
  }

  //tentativo login o registrarsi
  o.auth = function(username, signingUp) {

    var authRoute;

    if (signingUp) {
      authRoute = 'signup';
    } else {
      authRoute = 'login'
    }

    return $http.post(SERVER.url + '/' + authRoute, {username: username})
    // aggiornare il return di auth in moda da far si che riceva i dati dal server
      .sucess(function(data){
        o.setSession(data.username, data.session_id, data.favorites);
      });

  }

  //creo un metodo per aggiungere canzoni all'array
  o.addSongToFavorites = function(song) {
    //assicuriamoci che stiamo aggiungendo una canzone
    if(!song) return false;

    //aggiungiamo con unshift: metodo per aggiungere elementi ad un'array
    o.favorites.unshift(song);
    // Quando qualcuno aggiunge una canzone ai preferiti incrementa il valore di newFavorites
    o.newFavorites++;

    //collegare add song al server
    return $http.post(SERVER.url + '/favorites', {session_id: o.session_id, song_id:song.song_id });
  }

  //creao un metodo che ritorna il totale dei nuovi preferiti
  o.favoriteCount = function() {
    return o.newFavorites;
  }

  //Creiamo un metodo per rimouvere gli elementi dall'array
  o.removeSongFromFavorites = function(song, index) {

    //Assicuriamoci che aggiungiamo una canzone
    if(!song) return false;

    //utilizziamo splice per rimuovere gli elementi dall'array
    o.favorites.splice(index, 1);

    //collegare funzione al server
    return $http({
      method: 'DELETE',
      url: SERVER.url + '/favorites',
      params: { session_id: o.session_id, song_id:song.song_id }
    });
  }

  //ottenere l'intera lista favoriti dell'utente dal server
  o.populateFavorites = function() {
    return $http({
      method: 'GET',
      url: SERVER.url + '/favorites',
      params: { session_id: o.session_id }
    }).success(function(data){
      // inserire i dati in coda
      o.favorites = data;
   });
  }

  return o;
});
