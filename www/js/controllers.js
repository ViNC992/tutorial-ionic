angular.module('songhop.controllers', ['ionic', 'songhop.services'])


/*
Controller for the discover page
*/
.controller('DiscoverCtrl', function($scope, $timeout, User, Recommendations, $ionicLoading) {

  // funzione icona di caricamento
  var showLoading = function() {
    $ionicLoading.show({
      template: '<i class="ion-loading-c"></i>',
      noBackdrop: true
    });
  }

  var hideLoading = function() {
    $ionicLoading.hide();
  }

  // loading compare solo quando si stanno recuperando dati dal server
  showLoading();

  //La canzone partirà quando la pagina sarà caricata
  Recommendations.init()
    .then(function(){

      $scope.CurrentSong = Recommendations.queue[0];

      return Recommendations.playCurrentSong();
    })
    .then(function(){
      hideLoading();
      /* quando plpaycurrentSong è attiva(quindi riproduce la canzone corrente)
      impostare il currentSong.loaded su true coì ng-hide lo nasconde
      */
      $scope.currentSong.loaded = true;
    });

  // funzione per trovare la prossima immagine nell'album
  // Se non è disponibile l'immagine tornerà una stringa vuota
  $scope.nextAlbumImg = function() {
    if (Recommendations.queue.length > 1) {
      return Recommendations.queue[1].image_large;
    }

    return '';
  }
  // disponiamo le canzoni nella pagina
  Recommendations.getNextSongs()
    .then(function(){
      $scope.currentSong = Recommendations.queue[0];
    });

  //
  $scope.sendFeedback = function (bool) {

    //preparare la canzone successiva
    Recommendations.nextSong();

    //aggiungere elemento ai favoriti se viene cliccato
    if (bool) User.addSongToFavorites($scope.currentSong);

    // settare la corretta sequenza di animazioni
    $scope.currentSong.rated = bool;
    $scope.currentSong.hide = true;

    // La funzione viene applicata solo quando il tempo del timeout satà passato
    $timeout(function() {

      $scope.currentSong = Recommendations.queue[0];
      // settiamo lo stato di loaded su false in modo tale che l'icona sia visualizzata
      $scope.currentSong.loaded = false;
    }, 500);

    Recommendations.playCurrentSong().then(function() {
      // riattivo lo stato true quando
      $scope.currentSong.loaded = true;
    });

  }
})


/*
Controller for the favorites page
*/
.controller('FavoritesCtrl', function($scope, $window, User) {

  //metodo per aprire canzoni preferite su spotify
  $scope.openSong = function(song) {
    $window.open(song.open_url, "_system");
  }

  //Abilitare la visione dell'arary nella pagina
  $scope.favorites = User.favorites;

  //Importo il servizion nella pagina
  $scope.removeSong = function(song, index) {
    User.removeSongFromFavorites(song, index);
  }
})


/*
Controller for our tab bar
*/
.controller('TabsCtrl', function($scope, User, Recommendations) {

  //Esporre il numero di preferiti nel campo
  $scope.favCount = User.favoriteCount;

  // scope che permette di lanciare init quando viene chiamato on-deselect
  $scope.leavingFavorites = function() {
    Recommendations.init();
  }

  //blocchiamo l'audio quando andiamo sulla pagina dei preferiti
  $scope.enteringFavorites = function() {
    //impostiamo il conteggio dei favoriti su 0 quando fininamo nella pagina favoriti
    User.newFavorites = 0;
    Recommendations.haltAudio();
  }
})


/*
Controller for autentication
*/
.controller('SplashCtrl', function($scope, $state, User) {

  //tentavo di registrazione o login attraverso il metodo auth
  $scope.submitForm = function(username, signingUp) {
    User.auth(username, signingUp).then(function(){
      // reindirizziamo alla pagino discover
      $state.go('tab.discover');
    }, function() {
    // mesaggio di errore
    alert('Hmm... try another username.')
    });
  }

});
